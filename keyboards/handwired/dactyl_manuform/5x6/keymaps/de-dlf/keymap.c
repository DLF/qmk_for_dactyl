/* An ISO-DE (German) based layout. */ 

#include QMK_KEYBOARD_H
#include <eeprom.h>


#define _BASE 0
#define _NAVI 1
#define _SYMB 2
#define _META 3
#define CC_BIT_BASE 1
#define CC_BIT_NAVI 2
#define CC_BIT_SYMB 4
#define CC_BIT_META 8

#define NAVI TT(_NAVI)
#define SYMB TT(_SYMB)
#define META TO(_META)
#define TOBASE TO(_BASE)

rgblight_config_t col_base_layer;
rgblight_config_t col_navi_layer;
rgblight_config_t col_meta_layer;
rgblight_config_t col_symb_layer;
rgblight_config_t *active_color = 0;
uint8_t cc_keys = 0;

#define EECONFIG_COLOR_BASE_LAYER (uint32_t *)64
#define EECONFIG_COLOR_NAVI_LAYER (uint32_t *)68
#define EECONFIG_COLOR_SYMB_LAYER (uint32_t *)72
#define EECONFIG_COLOR_META_LAYER (uint32_t *)76

// Defines the keycodes used by our macros in process_record_user
enum custom_keycodes {
  CC_VU = SAFE_RANGE, // change color keys
  CC_VD,
  CC_SU,
  CC_SD,
  CC_HU,
  CC_HD,
  SC_BASE,          // set color keys
  SC_NAVI,
  SC_META,
  SC_SYMB
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  [_BASE] = LAYOUT_5x6(
     KC_ESC , KC_1  , KC_2  , KC_3  , KC_4  , KC_5  ,                         KC_6  , KC_7  , KC_8  , KC_9  , KC_0  ,KC_MINS,
     KC_TAB , KC_Q  , KC_W  , KC_E  , KC_R  , KC_T  ,                         KC_Y  , KC_U  , KC_I  , KC_O  , KC_P  ,KC_EQL,
     KC_LSFT, KC_A  , KC_S  , KC_D  , KC_F  , KC_G  ,                         KC_H  , KC_J  , KC_K  , KC_L  ,KC_NUHS,KC_RSFT,
     KC_LCTL, KC_Z  , KC_X  , KC_C  , KC_V  , KC_B  ,                         KC_N  , KC_M  ,KC_COMM,KC_DOT ,KC_SLSH,KC_RCTL,
                      KC_GRV,KC_NUBS,                                                        KC_CAPS,KC_RBRC,
                                     KC_LGUI, KC_SPC,                        KC_ENT,  KC_RGUI,
                                     SYMB,    NAVI,                          NAVI,    SYMB,
                                     KC_LALT, KC_DEL,                        KC_BSPC, KC_RALT
  ),
    
  [_NAVI] = LAYOUT_5x6(
     _______,KC_VOLU,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,                        KC_MPRV,KC_MNXT,XXXXXXX,KC_MPLY,KC_VOLU,XXXXXXX,
     _______,KC_VOLD,KC_HOME,KC_END, KC_PGUP,KC_PGDN,                        KC_HOME,KC_END, KC_PGUP,KC_PGDN,KC_VOLD,XXXXXXX,
     _______,KC_MUTE,KC_LEFT,KC_DOWN,KC_UP,  KC_RGHT,                        KC_LEFT,KC_DOWN,KC_UP,  KC_RGHT,KC_MUTE,_______,
     _______,KC_ACL0,KC_ACL1,KC_ACL2,KC_BTN3,KC_BTN2,                        KC_BTN1,KC_MS_L,KC_MS_D,KC_MS_U,KC_MS_R,_______,
                     META,   XXXXXXX,                                                        XXXXXXX,META   ,
                                             _______,_______,        _______,_______,
                                             _______,_______,        _______,_______,
                                             _______,_______,        _______,_______

  ),

  [_SYMB] = LAYOUT_5x6(
     KC_F12 , KC_F1 , KC_F2 , KC_F3 , KC_F4 , KC_F5 ,                        KC_F6  , KC_F7 , KC_F8 , KC_F9 ,KC_F10 ,KC_F11 ,
     KC_F13 ,KC_F14 ,KC_F15 ,KC_F16 ,KC_F17 , KC_F18,                        KC_PMNS,KC_KP_7,KC_KP_8,KC_KP_9,KC_PSLS,KC_NLCK,
     _______,XXXXXXX,KC_QUOT,KC_SCLN,KC_LBRC,XXXXXXX,                        KC_PPLS,KC_KP_4,KC_KP_5,KC_KP_6,KC_PAST,_______,
     _______,KC_INS, KC_PSCR,KC_SLCK,KC_PAUS,XXXXXXX,                        KC_PENT,KC_KP_1,KC_KP_2,KC_KP_3,KC_PENT,_______,
                     XXXXXXX,XXXXXXX,                                                        KC_KP_0,KC_PDOT,
                                             _______,_______,        _______,_______,
                                             _______,_______,        _______,_______,
                                             _______,_______,        _______,_______
  ),

  [_META] = LAYOUT_5x6(
       RESET,  XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,                        XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,RESET  ,
       XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,                        XXXXXXX,CC_HU,   CC_SU,   CC_VU,XXXXXXX,XXXXXXX,
       XXXXXXX,SC_META,SC_BASE,SC_NAVI,SC_SYMB,XXXXXXX,                        XXXXXXX,CC_HD,   CC_SD,   CC_VD,XXXXXXX,XXXXXXX,
       TOBASE, XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,                        XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,TOBASE ,
                       XXXXXXX,XXXXXXX,                                                        XXXXXXX,XXXXXXX,
                                             _______,_______,        _______,_______,
                                             _______,_______,        _______,_______,
                                             _______,_______,        _______,_______
  ),
};


//     XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,                        XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,
//     XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,                        XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,
//     XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,                        XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,
//     XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,                        XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,XXXXXXX,
//                     XXXXXXX,XXXXXXX,                                                        XXXXXXX,XXXXXXX,
//                                             XXXXXXX,XXXXXXX,        XXXXXXX,XXXXXXX,
//                                             XXXXXXX,XXXXXXX,        XXXXXXX,XXXXXXX,
//                                             XXXXXXX,XXXXXXX,        XXXXXXX,XXXXXXX
//


// set active color without storing it in EEPROM
void set_color(rgblight_config_t *col) {
  rgblight_sethsv_noeeprom(
    (*col).hue,
    (*col).sat,
    (*col).val
  );
}

// process a color change event
//
// keyrecord: the key event
// col: the color struct to manipulate (color object of a specific layer)
// cc_bit: bit code of the color object (layer) under manipulation 
// eeprom_address: the address in EEPROM where the color is stored
//
// uses global variables 
//  * "cc_keys" which identifies the latest manipulated layer color
//  * "active_color" which points to the color under manipulation
//
void color_key_event(
  keyrecord_t *record,
  rgblight_config_t *col,
  uint8_t cc_bit,
  uint32_t *eeprom_address
) {

  if (record->event.pressed) {
    // layer color key pressed - show that layer's color and note the layer in global var
    if (cc_keys == 0) {
        active_color = col;
        cc_keys |= cc_bit;
        set_color(active_color);
    }
  } else {
    // layer color key released - save the color and go back to color of meta layer
    if (cc_keys & cc_bit) {
        cc_keys ^= cc_bit;
        if (cc_keys == 0) {
            active_color = 0;
            eeprom_update_dword(eeprom_address, (*col).raw);
            set_color(&col_meta_layer);
        }
    }
  }
}


bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case SC_BASE:
      color_key_event(
        record,
        &col_base_layer,
        CC_BIT_BASE,
        EECONFIG_COLOR_BASE_LAYER
      );
      return true;
    case SC_NAVI:
      color_key_event(
        record,
        &col_navi_layer,
        CC_BIT_NAVI,
        EECONFIG_COLOR_NAVI_LAYER
      );
      return true;
    case SC_META:
      color_key_event(
        record,
        &col_meta_layer,
        CC_BIT_META,
        EECONFIG_COLOR_META_LAYER
      );
      return true;
    case SC_SYMB:
      color_key_event(
        record,
        &col_symb_layer,
        CC_BIT_SYMB,
        EECONFIG_COLOR_SYMB_LAYER
      );
      return true;
    case CC_VU:
      if (active_color) {
          active_color->val += 5;
          if (active_color->val > RGBLIGHT_LIMIT_VAL) {
              active_color->val = 0;
          }
          set_color(active_color);
      }
      return true;
    case CC_VD:
      if (active_color) {
          active_color->val -= 5;
          if (active_color->val > RGBLIGHT_LIMIT_VAL) {
              active_color->val = RGBLIGHT_LIMIT_VAL;
          }
          set_color(active_color);
      }
      return true;
    case CC_HU:
      if (active_color) {
          active_color->hue += 2;
          set_color(active_color);
      }
      return true;
    case CC_HD:
      if (active_color) {
          active_color->hue -= 4;
          set_color(active_color);
      }
      return true;
    case CC_SU:
      if (active_color) {
          active_color->sat += 5;
          set_color(active_color);
      }
      return true;
    case CC_SD:
      if (active_color) {
          active_color->sat -= 5;
          set_color(active_color);
      }
      return true;
  }
  return true;
}

void matrix_init_user(void) {
}

void matrix_scan_user(void) {
}

void led_set_user(uint8_t usb_led) {
}


layer_state_t layer_state_set_user(layer_state_t state) {
    switch (biton32(state)) {
    case _BASE:
        set_color(&col_base_layer);
        break;
    case _NAVI:
        set_color(&col_navi_layer);
        break;
    case _SYMB:
        set_color(&col_symb_layer);
        break;
    case _META:
        set_color(&col_meta_layer);
        break;
    default:
        rgblight_sethsv (HSV_CORAL);
        break;
    }
  return state;
}

void eeconfig_init_user(void) {
    col_symb_layer.hue = 11;
    col_symb_layer.sat = 176;
    col_symb_layer.val = 255;
    col_base_layer.hue = 0;
    col_base_layer.sat = 0;
    col_base_layer.val = 255;
    col_navi_layer.hue = 122;
    col_navi_layer.sat = 176;
    col_navi_layer.val = 255;
    col_meta_layer.hue = 0;
    col_meta_layer.sat = 255;
    col_meta_layer.val = 255;
    eeprom_update_dword(EECONFIG_COLOR_BASE_LAYER, col_base_layer.raw);
    eeprom_update_dword(EECONFIG_COLOR_NAVI_LAYER, col_navi_layer.raw);
    eeprom_update_dword(EECONFIG_COLOR_META_LAYER, col_meta_layer.raw);
    eeprom_update_dword(EECONFIG_COLOR_SYMB_LAYER, col_symb_layer.raw);
    rgblight_enable();
    rgblight_mode(1);
}

void keyboard_post_init_user(void) {
    col_base_layer.raw = eeprom_read_dword(EECONFIG_COLOR_BASE_LAYER);
    col_navi_layer.raw = eeprom_read_dword(EECONFIG_COLOR_NAVI_LAYER);
    col_meta_layer.raw = eeprom_read_dword(EECONFIG_COLOR_META_LAYER);
    col_symb_layer.raw = eeprom_read_dword(EECONFIG_COLOR_SYMB_LAYER);
    set_color(&col_base_layer);
}
